import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiserviceService } from '../apiservice.service';
import { Employee } from '../employee';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employees:Employee[];
  constructor(private employeeservice: ApiserviceService,private router:Router) { }

  ngOnInit(): void {
    this.getEmployees();
  }
  private getEmployees(){
    this.employeeservice.getEmployeeList().subscribe(data =>{
      this.employees = data;
    })
  }
  private updateEmployee(id: number){
    this.router.navigate(["/home/update-employee",id]);
  
  }
  deleteEmployee(id: number){
    this.employeeservice.deleteEmployee(id).subscribe( data => {
      console.log(data);
      this.getEmployees();
    })
  }
 

}
