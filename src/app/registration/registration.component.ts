import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiserviceService } from '../apiservice.service';
import { Registration } from '../registration';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
registration: Registration = new Registration();
  constructor(private service: ApiserviceService,private route: Router) { }


  saveRegister(){
    this.service.registerUser(this.registration).subscribe(data=>{
      console.log(data);
    },
    error=>console.log(error));
    this.toLogin();
  }


  toLogin(){
    this.route.navigate(["/login"]);
  }
  ngSubmit(){   
    this.saveRegister();
  }
  ngOnInit(): void {
    
  }

}
