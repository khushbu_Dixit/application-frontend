import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiserviceService } from '../apiservice.service';
import { Employee } from '../employee';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  employees: Employee= new Employee();
  constructor(private service:ApiserviceService,private router:Router) { }

  saveEmployee(){
    this.service.createEmployee(this.employees).subscribe(data=>{
      console.log(data);
    },
    error=>console.log(error));
    this.toEmployeeList();
  }


  toEmployeeList(){
    this.router.navigate(["/home/employees"]);
  }
  ngSubmit(){
    console.log(this.employees);   
    this.saveEmployee(); 
  }
  ngOnInit(): void {
  }

}
